# Maintainer: Felix Yan <felixonmars@archlinux.org>

_hkgname=polysemy
pkgname=haskell-polysemy
pkgver=1.9.2.0
pkgrel=14
pkgdesc="Higher-order, low-boilerplate, zero-cost free monads"
url="https://github.com/isovector/polysemy"
license=('BSD')
arch=('x86_64')
depends=('ghc-libs' 'haskell-async' 'haskell-first-class-families' 'haskell-syb'
         'haskell-th-abstraction' 'haskell-type-errors' 'haskell-unagi-chan')
makedepends=('ghc' 'uusi' 'haskell-cabal-doctest' 'haskell-hspec-discover' 'haskell-doctest' 'haskell-hspec'
             'haskell-inspection-testing')
source=("https://hackage.haskell.org/packages/archive/$_hkgname/$pkgver/$_hkgname-$pkgver.tar.gz")
sha512sums=('ed5ec5f210c1a7ca030b5891c98784ecfb468ab6c34ba2d877c837f5485d85764bbb7e3da6aa5fa8aaae021faa0b5c26739cb1b98f681dc954ea9fc38372a50b')

prepare() {
  cd $_hkgname-$pkgver
  uusi -u inspection-testing
}

build() {
  cd $_hkgname-$pkgver

  runhaskell Setup configure -O --enable-shared --enable-debug-info --enable-executable-dynamic --disable-library-vanilla \
    --prefix=/usr --docdir=/usr/share/doc/$pkgname --datasubdir=$pkgname --enable-tests \
    --dynlibdir=/usr/lib --libsubdir=\$compiler/site-local/\$pkgid \
    --ghc-option=-optl-Wl\,-z\,relro\,-z\,now \
    --ghc-option='-pie'

  runhaskell Setup build $MAKEFLAGS
  runhaskell Setup register --gen-script
  runhaskell Setup unregister --gen-script
  sed -i -r -e "s|ghc-pkg.*update[^ ]* |&'--force' |" register.sh
  sed -i -r -e "s|ghc-pkg.*unregister[^ ]* |&'--force' |" unregister.sh
}

check() {
  cd $_hkgname-$pkgver
  runhaskell Setup test --show-details=direct
}

package() {
  cd $_hkgname-$pkgver

  install -D -m744 register.sh "$pkgdir"/usr/share/haskell/register/$pkgname.sh
  install -D -m744 unregister.sh "$pkgdir"/usr/share/haskell/unregister/$pkgname.sh
  runhaskell Setup copy --destdir="$pkgdir"
  install -D -m644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
  rm -f "$pkgdir"/usr/share/doc/$pkgname/LICENSE
}
